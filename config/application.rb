require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Pluto
  class Application < Rails::Application
    # Silence deprecation warning after upgrade to Rails 4.0.2.
    # http://stackoverflow.com/questions/20361428/rails-i18n-validation-deprecation-warning
    config.i18n.enforce_available_locales = true
    config.time_zone = 'Brasilia'
    config.i18n.default_locale = :"pt_BR"
    config.use_plugin_parser = true
    config.log_formatter = ::Logger::Formatter.new

    # Observers
    #config.active_record.observers = :freight_observer

  end
end
