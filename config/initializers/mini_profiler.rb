begin
  Rack::MiniProfiler.config.pre_authorize_cb = lambda { |env| ENV['PLUTO_DISABLE_PROFILER'] != "true" }
  Rack::MiniProfiler.config.position         = 'right'
  Rack::MiniProfiler.config.start_hidden     = true
rescue
  Rails.logger.info("rack-mini-profiler not loaded")
end
