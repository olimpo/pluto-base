# Be sure to restart your server when you modify this file.

Pluto::Application.config.session_store :cookie_store, key: '_pluto_session'
#Pluto::Application.config.session_store :active_record_store, secure: Settings.read('environment.secure', default: false)