Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.sleep_delay         = 30
Delayed::Worker.max_attempts        = 5
Delayed::Worker.max_run_time        = 4.hours
Delayed::Worker.read_ahead          = 10
Delayed::Worker.default_queue_name  = 'default'
Delayed::Worker.delay_jobs = !Rails.env.test?
