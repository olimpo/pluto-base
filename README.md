# Pluto-Base
---------

Pluto, da mitologia grega, o Deus da riqueza ([http://pt.wikipedia.org/wiki/Pluto_(mitologia)](http://pt.wikipedia.org/wiki/Pluto_(mitologia))). Este nome foi dado pelo fato do sistema ser escrito em Ruby.

# Arquitetura Pluto

Este projeto é uma iniciativa open source para que os desenvolvedores ganhem tempo no startup de seus sistemas, tendo uma base consistente e de fácil expansão.

## Faça sua cópia

O Pluto é versionado com git, portanto, para fazer sua cópia do código e controlá-la, será necessário ter o git em sua máquina.

Há algum tempo eu fiz um "tutorialzinho" de alguns comandos git e para que eles servem: [http://macielbombonato.blogspot.com.br/2012/02/utilizando-git-versionador-local.html](http://macielbombonato.blogspot.com.br/2012/02/utilizando-git-versionador-local.html), Este tutorial deve ajudar um pouco para realizar operações via linha de comando.

## Crie seu Fork deste projeto

Caso queira montar um projeto a partir deste e queira deixá-lo versionado aqui no bitbucket, use a opção "Fork" que encontra-se no topo da tela. O processo é rápido e o passo a passo é bem simples.

Espero que este projeto lhe seja útil e tendo sugestões ou encontrando problemas, por favor, entre em contato ou abra uma issue aqui no bitbucket.

# Setup do Workspace

## Pré-requisitos

* Ruby 2.0
* Bundler
* MySQL server 5.1 ou posterior

## Setup do projeto

Configure a variável de ambiente `RAILS_ENV` em seu `~/.bash_profile` ou `~/.bashrc` ou `~/.profile` para `local`, pois será utilizado o ambiente `development` para o ambiente (real) de desenvolvimento do projeto.

    export RAILS_ENV=local

Configure também as variáveis de ambiente indicadas abaixo com preenchimento de exemplo para que o sistema tenha tudo certo para ser utilizado.

	export PLUTO_DB_NAME="pluto_local"
	export PLUTO_DB_USER="root"
	export PLUTO_DB_PASS=""
	export PLUTO_DB_HOST="127.0.0.1"
	export PLUTO_DB_PORT="3306"
	export PLUTO_DB_POOL_SIZE=5
	export PLUTO_LOG_LEVEL="info"

Em seguida instale as dependencias, inicialize o banco local e execute os testes. Lembrando que todos os pré-requisitos acima devem estar devidamente instalados

    bundle install
    rake db:setup
    rake pluto:occurrences:create_all
    rake spec

Se sua configuração de banco de dados local (p.ex. senha do usuário root), você pode utilizar variáveis de ambiente para sobrescrever as configurações do `config/database.yml`. Veja as variáveis disponíveis no próprio arquivo.

### Execução local

Para executar a aplicação localmente utilize o comando

    bundle exec rails server -e local

Se preverir declare a variável de ambiente `RAILS_ENV=local` em seu `~/.bash_profile` para não ter que declarar o environment sempre.

O arquivo `config/application.yml` lê diversos parâmetros de variáveis de ambiente, atribuindo valores default se não estiverem configuradas. Utilize essas variáveis em seu ambiente local para alterar as configurações, ao invés de alterar o arquivo `config/application.yml`.

O utilitário `bin/henv` exibe todas as variáveis de ambiente do Pluto configuradas no ambiente local.

---------
# Licença
---------

Pluto-Base é um software de código aberto, você pode redistribuí-lo e/ou modificá-lo conforme a licença Apache versão 2.0. Veja o arquivo LICENSE-Apache.txt
