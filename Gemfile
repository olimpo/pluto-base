source 'https://rubygems.org'

ruby '2.0.0'

gem 'rails', '4.0.3'
gem 'mysql2'
gem 'activerecord-session_store'
gem 'mongoid', git: 'https://github.com/mongoid/mongoid.git'
gem 'yajl-ruby'

gem 'therubyracer', platforms: :ruby
gem "less-rails"
gem 'twitter-bootstrap-rails'

gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'
gem 'httparty'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0.0'
gem 'will_paginate', git: 'https://github.com/mislav/will_paginate.git', branch: :master
gem 'will_paginate-bootstrap'
gem 'simple_form', '~> 3.0.0.rc'
gem 'roo'
gem 'json-schema'
gem 'savon', '~> 2.0'
gem 'settingslogic'
gem 'devise'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'gelf'
gem 'graylog2_exceptions'
gem 'br-cnpj', require: 'br/cnpj'
gem 'whenever', require: false
gem 'fpm', require: false
gem 'time_diff'
gem 'nokogiri'
gem 'nori'
gem 'rails-observers'
gem 'htmlentities'
gem 'ice_cube'

# Use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :local, :test do
  gem 'quiet_assets'
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'awesome_print'
  gem 'rspec-rails', '~> 2.0'
  gem 'factory_girl_rails'
  gem 'mixlib-cli'
  gem 'brakeman', require: false
  gem 'cpf_faker'
  gem 'metric_fu'
end

group :local, :development, :qa, :stage do
  gem 'rack-mini-profiler'
end

group :test do
  gem 'simplecov'
  gem 'simplecov-rcov'
  gem 'cucumber-rails', require: false
  gem 'database_cleaner'
  gem 'shoulda-matchers'
  gem 'restful-matchers'
  gem 'capybara'
  gem 'selenium-webdriver', '~> 2.35.1'
  gem 'webmock', '1.11.0'
  gem 'vcr'
  gem 'launchy'
  gem 'timecop'
  gem 'autotest-rails'
  gem 'autotest-notification'
  gem 'json_spec'
  gem 'curb'
end
